from .Node import Node
from typing import Optional


class DoublyLinkedList:
    def __init__(self, head=None):
        self.head: Node = head

    def insert_at_head(self, node):
        self.insert(None, node, self.head)

    def insert_before(self, data, node):
        node_after = self.find_node(data)
        node_before = node_after.prev_node
        self.insert(node_before, node, node_after)

    def insert_after(self, data, node):
        node_before = self.find_node(data)
        node_after = node_before.next_node
        self.insert(node_before, node, node_after)

    # where position 1 is the head node
    def insert_at_position(self, position, node):
        index = 1
        curr = self.head
        while index < position:
            curr = curr.next_node
            index = index + 1
        self.insert_before(curr.data, node)

    def delete(self, data):
        to_delete = self.find_node(data)
        before = to_delete.prev_node
        after = to_delete.next_node
        if before is not None:
            before.next_node = after
        if after is not None:
            after.prev_node = before

    def find_node(self, data):
        curr = self.head
        while curr is not None:
            if curr.data is data:
                return curr
            curr = curr.next_node
        raise ValueError(f'{data} is not in the list')

    @staticmethod
    def insert(before: Optional[Node], node: Node, after: Optional[Node]):
        if before is not None:
            before.next_node = node
            node.prev_node = before
        if after is not None:
            after.prev_node = node
            node.next_node = after
