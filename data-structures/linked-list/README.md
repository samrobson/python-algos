# Linked Lists
![Doubly Linked List](DoublyLinkedList.png)

## Complexities

### Time

| Access | Search | Insert  | Delete  |
|---|---|---|---|
| O(n) | O(n) | O(1) | O(n) |

### Space
O(n)