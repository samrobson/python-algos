import unittest
from ..Node import Node
from ..DoublyLinkedList import DoublyLinkedList


class MyTestCase(unittest.TestCase):
    def test_insert_after(self):
        head = Node(1)
        linked_list = DoublyLinkedList(head)

        first = Node(2)
        second = Node(3)

        linked_list.insert_after(head.data, first)
        linked_list.insert_after(first.data, second)

        self.assert_equal_list(head, [1, 2, 3])

    def test_insert_before(self):
        head = Node(1)
        linked_list = DoublyLinkedList(head)

        tail = Node(50)
        linked_list.insert_after(head.data, tail)

        first = Node(2)
        second = Node(3)

        linked_list.insert_before(tail.data, second)
        linked_list.insert_before(second.data, first)

        self.assert_equal_list(head, [1, 2, 3, 50])

    def test_insert_at_head(self):
        head = Node(1)
        linked_list = DoublyLinkedList(head)

        new_head = Node(2)
        linked_list.insert_at_head(new_head)
        self.assert_equal_list(new_head, [2, 1])

    def test_insert_at_position(self):
        head = Node('h')
        linked_list = DoublyLinkedList(head)

        first = Node(2)
        second = Node("sec")

        linked_list.insert_after(head.data, first)
        linked_list.insert_after(first.data, second)

        third = Node({1, 2})
        linked_list.insert_at_position(2, third)

        self.assert_equal_list(head, ['h', {1, 2}, 2, "sec"])

    def test_delete(self):
        head = Node('h')
        linked_list = DoublyLinkedList(head)

        first = Node(2)
        linked_list.insert_after(head.data, first)
        linked_list.delete(2)

        self.assert_equal_list(head, ['h'])

    # Helper method that asserts a list contains the right sequence of elements
    def assert_equal_list(self, list_head: Node, expected_list: list):
        elements = []
        curr_node = list_head
        while curr_node is not None:
            elements.append(curr_node.data)
            curr_node = curr_node.next_node
        self.assertListEqual(elements, expected_list)


if __name__ == '__main__':
    unittest.main()
